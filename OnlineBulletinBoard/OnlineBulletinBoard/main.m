//
//  main.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/9/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
