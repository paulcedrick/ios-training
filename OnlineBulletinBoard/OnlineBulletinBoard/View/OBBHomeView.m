//
//  OBBHomeView.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/10/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBHomeView.h"

@implementation OBBHomeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)btnLogoutClicked:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedLogout)]) {
        [self.delegate clickedLogout];
    }
}

- (IBAction)btnAddBullet:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addPost)]) {
        [self.delegate addPost];
    }
}
@end
