//
//  OBBPostViewForm.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OBBPostViewFormDelegate <NSObject>

@required

- (void) btnAddPost;

@end


@interface OBBPostViewForm : UIView
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnPost;
@property (weak, nonatomic) IBOutlet UITextField *addTxtTitle;
@property (weak, nonatomic) IBOutlet UITextView *addTxtDesc;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)btnAddPost:(id)sender;

@property (strong) id<OBBPostViewFormDelegate> delegate;
@end
