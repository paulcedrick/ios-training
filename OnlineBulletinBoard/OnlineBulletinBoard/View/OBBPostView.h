//
//  OBBPostView.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OBBPostViewAddCommentDelegate <NSObject>

@required

- (void) btnAddCommentLink;

@end

@interface OBBPostView : UIView
@property (weak, nonatomic) IBOutlet UILabel *txtTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtDetail;
@property (weak, nonatomic) IBOutlet UILabel *txtAuthorName;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet UIButton *btnAddComment;
@property (weak, nonatomic) IBOutlet UITableView *tblComment;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)btnAddCommentLink:(id)sender;

@property (strong) id<OBBPostViewAddCommentDelegate> delegate;
@end
