//
//  OBBCommentTableViewCell.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBBCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *txtUser;
@property (weak, nonatomic) IBOutlet UITextView *txtCommentContent;

@end
