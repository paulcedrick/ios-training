//
//  OBBPostTableViewCell.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/10/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBBPostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *txtPostTitle;
@property (weak, nonatomic) IBOutlet UILabel *txtPostDesc;
@property (weak, nonatomic) IBOutlet UILabel *txtPostAuthor;

@end
