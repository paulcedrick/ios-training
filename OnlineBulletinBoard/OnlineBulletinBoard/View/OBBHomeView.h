//
//  OBBHomeView.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/10/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OBBHomeViewDelegate <NSObject>

@required

- (void) addPost;

- (void) clickedLogout;

@end

@interface OBBHomeView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgPhotoThumb;
@property (weak, nonatomic) IBOutlet UILabel *txtName;
@property (weak, nonatomic) IBOutlet UITableView *tblList;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

- (IBAction)btnLogoutClicked:(id)sender;
- (IBAction)btnAddBullet:(id)sender;

@property (strong) id<OBBHomeViewDelegate> delegate;
@end
