//
//  OBBLoginView.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/9/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OBBLoginViewDelegate <NSObject>

@required

- (void) loginButtonPressed;

@end

@interface OBBLoginView : UIView

@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)loginButtonPressed:(id)sender;

@property (strong) id<OBBLoginViewDelegate> delegate;
@end
