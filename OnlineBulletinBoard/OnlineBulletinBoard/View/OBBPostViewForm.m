//
//  OBBPostViewForm.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBPostViewForm.h"

@implementation OBBPostViewForm

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)btnAddPost:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnAddPost)]) {
        [self.delegate btnAddPost];
    }
}
@end
