//
//  OBBPostTableViewCell.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/10/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBPostTableViewCell.h"

@implementation OBBPostTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
