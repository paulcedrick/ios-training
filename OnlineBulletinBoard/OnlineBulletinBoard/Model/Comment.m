//
//  Comment.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "Comment.h"
#import "Post.h"


@implementation Comment

@dynamic comment_txt;
@dynamic comment_id;
@dynamic comment_user;
@dynamic post;

@end
