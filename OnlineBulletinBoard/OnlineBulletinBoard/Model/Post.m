//
//  Post.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "Post.h"
#import "Comment.h"


@implementation Post

@dynamic body;
@dynamic title;
@dynamic user;
@dynamic comment;

@end
