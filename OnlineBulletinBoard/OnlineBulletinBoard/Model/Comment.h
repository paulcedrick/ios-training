//
//  Comment.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Post;

@interface Comment : NSManagedObject

@property (nonatomic, retain) NSString * comment_txt;
@property (nonatomic, retain) NSString * comment_id;
@property (nonatomic, retain) NSString * comment_user;
@property (nonatomic, retain) Post *post;

@end
