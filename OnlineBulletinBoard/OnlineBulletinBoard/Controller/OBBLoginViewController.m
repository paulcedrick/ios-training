//
//  OBBLoginViewController.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/9/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBLoginViewController.h"

@interface OBBLoginViewController () {
    UITextField *activeField;
}
@end

@implementation OBBLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.loginView = (OBBLoginView*)[self getCustomXibUsingXibName:@"LoginView"];
    
    //Delegates
    self.loginView.delegate = self;
    self.loginView.txtUsername.delegate = self;
    self.loginView.txtPassword.delegate = self;
    
    [self registerForKeyboardNotifications];
    
    self.users = [[NSArray alloc] initWithObjects:@"earth", @"vaughn", nil];
    
    self.passwords = [[NSMutableArray alloc] initWithObjects:@"maniebo", @"cute", nil];
    
    [self.view addSubview:self.loginView];
    
    self.loginView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - EBB LoginView Delegates
- (void)loginButtonPressed {
    NSLog(@"Hello Man!");
    
    
    
    NSString *inputUser = self.loginView.txtUsername.text;
    NSString *inputPassword = self.loginView.txtPassword.text;
    
    //Check if textfields have value
    if (![inputUser isEqual:@""] && ![inputPassword isEqual:@""]) {
        if ([self.users containsObject:inputUser] && [self.passwords containsObject:inputPassword]) {
            NSInteger userIndex = [self.users indexOfObject:inputUser];
            NSInteger passwordIndex = [self.passwords indexOfObject:inputPassword];
            
            if (userIndex == passwordIndex) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:inputUser forKey:@"username"];
                [self performSegueWithIdentifier:@"LoginToHomeSegue" sender:self];
            }
            else {
                NSString *msg = @"Wrong Username or Password";
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alertView show];
            }
        }
        else {
            NSString *msg = @"Wrong Username or Password";
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
    else {
        NSString *msg = @"Input Username or Password";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark - UITextfield Delegates
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    activeField = textField;
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    activeField = nil;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Keyboard
- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    // Get size of displayed keyboard
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // Compute visible active field
    CGRect visibleActiveFieldRect = CGRectMake(activeField.frame.origin.x, activeField.frame.origin.y + kbSize.height, activeField.frame.size.width, activeField.frame.size.height);
    
    // Adjust scroll view content size
    self.loginView.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + kbSize.height);
    
    // Scroll to visible active field
    [self.loginView.scrollView scrollRectToVisible:visibleActiveFieldRect animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    // Reset the content size of the scroll view
    self.loginView.scrollView.contentSize = CGSizeMake(0.0, 0.0);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
