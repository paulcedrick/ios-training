//
//  OBBHomeViewController.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/10/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBHomeViewController.h"

@interface OBBHomeViewController () {
    NSMutableArray *titles;
    NSMutableArray *posts;
    NSMutableArray *users;
}

@end

@implementation OBBHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.hidesBackButton = YES;
    
    self.homeView = (OBBHomeView*)[self getCustomXibUsingXibName:@"HomeView"];
    
    //Self delegates
    self.homeView.delegate = self;
    self.homeView.tblList.delegate = self;
    self.homeView.tblList.dataSource = self;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *username = [defaults objectForKey:@"username"];
    NSLog([defaults objectForKey:@"username"]);
    self.homeView.txtName.text = username;
    
    if ([username isEqualToString:@"earth"]) {
        self.homeView.imgPhotoThumb.image = [UIImage imageNamed:@"earth"];
    }
    else {
        self.homeView.imgPhotoThumb.image = [UIImage imageNamed:@"vaughn"];
    }
    
    [self.view addSubview:self.homeView];
    
    self.homeView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    
//    if ([defaults objectForKey:@"isFirstRun"] == nil) {
//        
//        [defaults setObject:@"YES" forKey:@"isFirstRun"];
//        
//        //Init Arrays
//        titles = [[NSMutableArray alloc] initWithObjects:@"First", @"Second", @"Third", nil];
//        
//        posts = [[NSMutableArray alloc] initWithObjects:@"First Post", @"Second Post", @"Third Post", nil];
//        
//        users = [[NSMutableArray alloc] initWithObjects:@"user1", @"user2", @"user3", nil];
//        
//        [defaults setObject:titles forKey:@"titles"];
//        [defaults setObject:posts forKey:@"posts"];
//        [defaults setObject:users forKey:@"users"];
//        [defaults synchronize];
//    }

}

//- (void) viewDidAppear:(BOOL)animated{
//    [self.homeView.tblList reloadData];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
//    
//    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSEntityDescription *entityPost = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
//    NSManagedObject *newPost = [[NSManagedObject alloc] initWithEntity:entityPost insertIntoManagedObjectContext:ad.managedObjectContext];
    
    
    
//    return persistentTitles.count;
    return [self getAllPost].count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.homeView.tblList) {
        OBBPostTableViewCell *postCell = (OBBPostTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"PostCell"];
        
        if (postCell == nil) {
            postCell = [[[NSBundle mainBundle] loadNibNamed:@"PostTableViewCell" owner:nil options:nil]objectAtIndex:0];
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSMutableArray *persistentTitle = [[defaults arrayForKey:@"titles"] mutableCopy];
//        NSMutableArray *persistentPosts = [[defaults arrayForKey:@"posts"] mutableCopy];
//        NSMutableArray *persistentUsers = [[defaults arrayForKey:@"users"] mutableCopy];
        
        Post *post = [[self getAllPost] objectAtIndex:indexPath.row];
        
//        postCell.txtPostTitle.text = [persistentTitle objectAtIndex:indexPath.row];
//        
//        postCell.txtPostDesc.text = [persistentPosts objectAtIndex:indexPath.row];
//        
//        postCell.txtPostAuthor.text = [persistentUsers objectAtIndex:indexPath.row];
        
        postCell.txtPostTitle.text = post.title;
        postCell.txtPostDesc.text = post.body;
        postCell.txtPostAuthor.text = post.user;
        
        return postCell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *row = @(indexPath.row).stringValue;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:row forKey:@"index"];
    [defaults setObject:@"view" forKey:@"open"];
    [self performSegueWithIdentifier:@"HomeToPostSegue" sender:self];
}

- (void)clickedLogout {
    NSLog(@"YES MAN");
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addPost {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"add" forKey:@"open"];
    
    [self performSegueWithIdentifier:@"HomeToPostSegue" sender:self];
    
}

-(NSArray*)getAllPost {
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
//    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"user", user];
//    [fetchRequest setPredicate:predicate];

    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [ad.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return result;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
