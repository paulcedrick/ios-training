//
//  OBBPostViewController.m
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBPostViewController.h"

@interface OBBPostViewController () {
        UITextView *activeField;
}

@end

@implementation OBBPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *gateway = [defaults objectForKey:@"open"];
    

    
    if ([gateway isEqualToString:@"view"]) {
        self.postView = (OBBPostView*)[self getCustomXibUsingXibName:@"PostView"];
        NSString *rowNum = [defaults objectForKey:@"index"];
        
        NSInteger row = [rowNum intValue];
        
//        NSMutableArray *titles = [defaults objectForKey:@"titles"];
//        NSMutableArray *authors = [defaults objectForKey:@"users"];
//        NSMutableArray *posts = [defaults objectForKey:@"posts"];
//        
//        NSString *title = [titles objectAtIndex:row];
//        NSString *author = [authors objectAtIndex:row];
//        NSString *post = [posts objectAtIndex:row];
        
        Post *post = [[self getAllPost]objectAtIndex:row];
        
        self.postView.txtTitle.text = post.title;
        self.postView.txtDetail.text = post.body;
        self.postView.txtAuthorName.text = post.user;
        self.postView.delegate = self;
        self.postView.tblComment.delegate = self;
        self.postView.tblComment.dataSource = self;
        self.postView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
        self.globalView = self.postView;
    }
    else {
        self.postViewForm = (OBBPostViewForm*)[self getCustomXibUsingXibName:@"PostViewForm"];
        self.postViewForm.delegate = self;
        self.postViewForm.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
        self.globalView = self.postViewForm;
        
    }
//    [self registerForKeyboardNotifications];
//    self.globalView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.globalView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - Keyboard
//- (void)registerForKeyboardNotifications {
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
//}
//
//- (void)keyboardWasShown:(NSNotification *)aNotification {
//    // Get size of displayed keyboard
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    // Compute visible active field
//    CGRect visibleActiveFieldRect = CGRectMake(activeField.frame.origin.x, activeField.frame.origin.y + kbSize.height, activeField.frame.size.width, activeField.frame.size.height);
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *gateway = [defaults objectForKey:@"open"];
//    
//    if ([gateway isEqualToString:@"view"]) {
//        // Adjust scroll view content size
//        self.postView.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + kbSize.height);
//        
//        // Scroll to visible active field
//        [self.postView.scrollView scrollRectToVisible:visibleActiveFieldRect animated:YES];
//    }
//    else {
//        // Adjust scroll view content size
//        self.postView.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + kbSize.height);
//        
//        // Scroll to visible active field
//        [self.postView.scrollView scrollRectToVisible:visibleActiveFieldRect animated:YES];
//    }
//    
//   
//}
//
//- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
//    // Reset the content size of the scroll view
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *gateway = [defaults objectForKey:@"open"];
//    
//    if ([gateway isEqualToString:@"view"]) {
//        self.postView.scrollView.contentSize = CGSizeMake(0.0, 0.0);
//    }
//    else {
//        self.postViewForm.scrollView.contentSize = CGSizeMake(0.0, 0.0);
//    }
//
//}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    NSMutableArray *persistentTitles = [[defaults arrayForKey:@"titles"] mutableCopy];
    //
    //    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //    NSEntityDescription *entityPost = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
    //    NSManagedObject *newPost = [[NSManagedObject alloc] initWithEntity:entityPost insertIntoManagedObjectContext:ad.managedObjectContext];
    
    
    
    //    return persistentTitles.count;
    return [self getAllComment].count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.postView.tblComment) {
        OBBCommentTableViewCell *postCell = (OBBCommentTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
        
        if (postCell == nil) {
            postCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentViewCell" owner:nil options:nil]objectAtIndex:0];
        }
        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //        NSMutableArray *persistentTitle = [[defaults arrayForKey:@"titles"] mutableCopy];
        //        NSMutableArray *persistentPosts = [[defaults arrayForKey:@"posts"] mutableCopy];
        //        NSMutableArray *persistentUsers = [[defaults arrayForKey:@"users"] mutableCopy];
        
        Comment *comment = [[self getAllComment] objectAtIndex:indexPath.row];
        
        //        postCell.txtPostTitle.text = [persistentTitle objectAtIndex:indexPath.row];
        //
        //        postCell.txtPostDesc.text = [persistentPosts objectAtIndex:indexPath.row];
        //
        //        postCell.txtPostAuthor.text = [persistentUsers objectAtIndex:indexPath.row];
        
        postCell.txtUser.text = comment.comment_user;
        postCell.txtCommentContent.text = comment.comment_txt;
        
        return postCell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *row = @(indexPath.row).stringValue;
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:row forKey:@"index"];
//    [defaults setObject:@"view" forKey:@"open"];
//    [self performSegueWithIdentifier:@"HomeToPostSegue" sender:self];
}


- (void) btnAddPost {
    NSLog(@"YO");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSEntityDescription *entityPost = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
    NSManagedObject *newPost = [[NSManagedObject alloc] initWithEntity:entityPost insertIntoManagedObjectContext:ad.managedObjectContext];
    BOOL flag = YES;

//    NSMutableArray *adtitles = [defaults objectForKey:@"titles"];
//    NSMutableArray *adposts = [defaults objectForKey:@"posts"];
//    NSMutableArray *adauthors = [defaults objectForKey:@"users"];
    NSString *newTitle = nil;
    NSString *newDesc = nil;
    if ([self.postViewForm.addTxtTitle.text isEqualToString:@""]) {
        flag = NO;
    }
    else {
        newTitle = self.postViewForm.addTxtTitle.text;
        if ([self.postViewForm.addTxtDesc.text isEqualToString:@""]) {
            flag = NO;
        }
        else {
            newDesc = self.postViewForm.addTxtDesc.text;
        }
    }


    NSString *username = [defaults objectForKey:@"username"];
    NSLog(username);
    if (flag) {
        [newPost setValue:newTitle forKey:@"title"];
        [newPost setValue:newDesc forKey:@"body"];
        [newPost setValue:username forKey:@"user"];
        
        NSError *error = nil;
        
        if (![newPost.managedObjectContext save:&error]) {
            NSLog(@"error");
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        NSString *msg = @"Please fill the title field and the description field";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
    
//    [adtitles addObject:newTitle];
//    [adposts addObject:newDesc];
//    [adauthors addObject:username];
}

-(void)btnAddCommentLink {
    NSLog(@"yo!!!!");
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSEntityDescription *entityComment = [NSEntityDescription entityForName:@"Comment" inManagedObjectContext:ad.managedObjectContext];
    NSManagedObject *newComment = [[NSManagedObject alloc] initWithEntity:entityComment insertIntoManagedObjectContext:ad.managedObjectContext];
    
    BOOL flag = YES;
    
    if ([self.postView.txtComment.text isEqualToString:@""]) {
        flag = NO;
    }
    
    if (flag) {
        NSString *comment = self.postView.txtComment.text;
        NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
        NSString *postId = [[NSUserDefaults standardUserDefaults] objectForKey:@"index"];
        
        [newComment setValue:user forKey:@"comment_user"];
        [newComment setValue:comment forKey:@"comment_txt"];
        [newComment setValue:postId forKey:@"comment_id"];
        
        NSError *error = nil;
        
        if (![newComment.managedObjectContext save:&error]) {
            NSLog(@"error");
        }
        
        [self.postView.tblComment reloadData];
    }
    else {
        NSString *msg = @"Please fill the comments field if you want to add a comment";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
    
}

-(NSArray*)getAllPost {
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

//    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"user", user];
//    [fetchRequest setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Post" inManagedObjectContext:ad.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [ad.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return result;
}

-(NSArray*)getAllComment {
    AppDelegate *ad = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    NSString *postId = [[NSUserDefaults standardUserDefaults] objectForKey:@"index"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"comment_id", postId];
    [fetchRequest setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Comment" inManagedObjectContext:ad.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [ad.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return result;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
