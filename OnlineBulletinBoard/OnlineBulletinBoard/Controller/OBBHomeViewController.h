//
//  OBBHomeViewController.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/10/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBBaseUIViewController.h"
#import "OBBHomeView.h"
#import "OBBPostTableViewCell.h"
#import "AppDelegate.h"
#import "Post.h"

@interface OBBHomeViewController : OBBBaseUIViewController<OBBHomeViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) OBBHomeView *homeView;
//
//@property (strong, nonatomic) NSMutableArray *title;
//@property (strong, nonatomic) NSMutableArray *posts;
//@property (strong, nonatomic) NSMutableArray *users;


@end
