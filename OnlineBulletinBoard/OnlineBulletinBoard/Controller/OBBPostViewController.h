//
//  OBBPostViewController.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/11/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBBaseUIViewController.h"
#import "OBBPostView.h"
#import "OBBPostViewForm.h"
#import "AppDelegate.h"
#import "Post.h"
#import "Comment.h"
#import "OBBCommentTableViewCell.h"

@interface OBBPostViewController : OBBBaseUIViewController<OBBPostViewFormDelegate, OBBPostViewAddCommentDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) OBBPostView *postView;
@property (strong, nonatomic) OBBPostViewForm *postViewForm;
@property (strong, nonatomic) UIView *globalView;


@end
