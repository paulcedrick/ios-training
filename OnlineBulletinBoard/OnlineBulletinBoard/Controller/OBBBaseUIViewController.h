//
//  OBBBaseUIViewController.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/9/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBBBaseUIViewController : UIViewController
- (UIView*)getCustomXibUsingXibName:(NSString*)xibName;
@end
