//
//  OBBLoginViewController.h
//  OnlineBulletinBoard
//
//  Created by Paul Cedrick on 12/9/14.
//  Copyright (c) 2014 Paul Cedrick. All rights reserved.
//

#import "OBBBaseUIViewController.h"
#import "OBBLoginView.h"

@interface OBBLoginViewController : OBBBaseUIViewController<OBBLoginViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) OBBLoginView *loginView;
@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) NSMutableArray *passwords;


@end
